# Helpful Links
- Web based z3 Solver: https://rise4fun.com/z3
- smtlib standard: http://smtlib.cs.uiowa.edu/standard.shtml 
  - Languages: http://smtlib.cs.uiowa.edu/papers/smt-lib-reference-v2.6-r2017-07-18.pdf
  - Theories: http://smtlib.cs.uiowa.edu/theories.shtml
  - Logics: http://smtlib.cs.uiowa.edu/logics.shtml
- BanditFuzz rewrite template: https://pastebin.com/jv1Tbkgb 
